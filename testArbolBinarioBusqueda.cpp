#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include "libs/ArbolBinarioBusqueda.hpp"

using namespace std;

void get_randomData(vector<int> & datas, int n){
	srand(time(0));
	for(int i = 0; i < n; i++){
		datas.push_back(i);
	}
	random_shuffle(datas.begin(), datas.end());
}

void get_sequentialData(vector<int> & datas, int n){
	for(int i = 0; i < n; i++){
		datas.push_back(i);
	}
}

int main(int argc, char * argv[]){
	cout << "Hola Mundo!" << endl;
	
	ArbolBinarioBusqueda arbol;
		
	int cant;
	cin >> cant;
	
	int valor;
	for(int i = 0; i < cant; i++){
		cin >> valor;
		if(arbol.insertar(valor)){
			cout << "El "<<valor<<" fue insertado" << endl;
		}else{
			cout << "El "<<valor<<" no fue insertado" << endl;
		}
	}
	if(arbol.insertar(valor)){
		cout << "El "<<valor<<" fue insertado" << endl;
	}else{
		cout << "El "<<valor<<" no fue insertado" << endl;
	}
	
	valor = 6;
	if(arbol.buscar(valor) != NULL){
		cout << "El "<<valor<<" fue encontrado" << endl;
	}else{
		cout << "El "<<valor<<" no fue encontrado" << endl;
	}
	
	valor = 12;
	if(arbol.buscar(valor) != NULL){
		cout << "El "<<valor<<" fue encontrado" << endl;
	}else{
		cout << "El "<<valor<<" no fue encontrado" << endl;
	}
	
	arbol.inOrder();
	Nodo * nodo;
	
	for(int i = 0; i < cant; i++){
		nodo = arbol.predecesor(i);
		if(nodo != NULL){
			cout << "El predecesor de "<<i<<" es " << nodo->valor << endl;
		}else{
			cout << ""<<i<<" no tiene predecesor" << endl;
		}
	}
	
	/*
	cout << "Casos más grandes" << endl;
	
	vector <int> ordenados;
	vector <int> desordenados;
	
	int tam;
	cin >> tam;
	get_sequentialData(ordenados, tam);
	get_randomData(desordenados, tam);
	ArbolBinarioBusqueda arbolOrdenado;
	ArbolBinarioBusqueda arbolDesordenado;
	
	long start = clock();
	for(int i; i < tam; i++){
		arbolOrdenado.insertar(ordenados[i]);
		//cout << ordenados[i] << " insertado" << endl;
	}
	long finish = clock();
	long orderTime = ((finish-start)/CLOCKS_PER_SEC) * 1000000;
	
	start = clock();
	for(int i; i < tam; i++){
		arbolDesordenado.insertar(desordenados[i]);
		//cout << desordenados[i] << " insertado" << endl;
	}
	finish = clock();
	long desorderTime = ((finish-start)/CLOCKS_PER_SEC) * 1000000;
	
	cout << orderTime << " microsecond vs " << desorderTime <<" microseconds" << endl;
	*/
	return 0;
}