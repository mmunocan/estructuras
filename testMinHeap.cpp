#include <iostream>
#include "libs/MinHeap.hpp"

using namespace std;

int main(int argc, char * argv[]){
	cout << "Hola Mundo!" << endl;
	MinHeap h;
	int min = h.buscarMin();
	cout << "El minimo encontrado es: " << min << endl;
	
	int n, valor;
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> valor;
		cout << "Voy a insertar el " << valor << endl;
		h.insert(valor);
		min = h.buscarMin();
		cout << "El minimo encontrado es: " << min << endl;
	}
	
	for(int i = 0; i < n; i++){
		cout << "Voy a eliminar " << endl;
		h.borrarMin();
		min = h.buscarMin();
		cout << "El minimo encontrado es: " << min << endl;
	}
	
	return 0;
}