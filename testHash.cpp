#include <iostream>
#include "libs/Hash.hpp"

using namespace std;

int main(int argc, char * argv[]){
	cout << "Hola Mundo!" << endl;
	
	Hash h;
	
	int valor;
	while(cin >> valor){
		h.insertar(valor);
		cout << "He insertado el " << valor << endl;
		cout << "Mi tabla tiene " << h.getCantidad() << " elementos" << endl;
	}
	
	return 0;
}