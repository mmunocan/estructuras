CPP=g++

OBJECTS=libs/ArbolBinarioBusqueda.o libs/MinHeap.o libs/Hash.o libs/MatrixGraph.o libs/LinkedGraph.o\
		libs/WeightGraph.o libs/UnionFind.o
		
BINS=testArbolBinarioBusqueda testMinHeap testHash testGraph testWeightGraph\
		
CPPFLAGS=-std=c++11 -O3 -DNDEBUG
DEST=.

%.o: %.c
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: clean bin

bin: $(OBJECTS) $(BINS)

testArbolBinarioBusqueda:
	g++ $(CPPFLAGS) -o $(DEST)/testArbolBinarioBusqueda testArbolBinarioBusqueda.cpp $(OBJECTS) -lm
	
testMinHeap:
	g++ $(CPPFLAGS) -o $(DEST)/testMinHeap testMinHeap.cpp $(OBJECTS) -lm
	
testHash:
	g++ $(CPPFLAGS) -o $(DEST)/testHash testHash.cpp $(OBJECTS) -lm
	
testGraph:
	g++ $(CPPFLAGS) -o $(DEST)/testGraph testGraph.cpp $(OBJECTS) -lm
	
testWeightGraph:
	g++ $(CPPFLAGS) -o $(DEST)/testWeightGraph testWeightGraph.cpp $(OBJECTS) -lm
	
clean:
	rm -f $(OBJECTS) $(BINS)
	cd $(DEST); rm -f *.a $(BINS)

