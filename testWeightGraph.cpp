#include <iostream>
#include <fstream>
#include "libs/WeightGraph.hpp"

using namespace std;

int main(int argc, char * argv[]){
	if(argc != 2){
		cout << "USO: " << argv[0] << " <FILENAME>" << endl;
		return -1;
	}
	ifstream input(argv[1]);
	if(!input.is_open()){
		cout << "Error al abrir el archivo" << argv[1] << endl;
		return -1;
	}
	
	int nodos, p, q, w;
	input >> nodos;
	
	WeightGraph wg(nodos);
	
	cout << "=========================" << endl;
	cout << "Inserciones: " << endl;
	cout << "=========================" << endl;
	
	while(input >> p >> q >> w){
		if(wg.insertar(p, q, w)){
			cout << "Inserté el par (" << p << ", " << q << ") peso "<<w<<" al WeightGraph" << endl;
		}else{
			cout << "No pude insertar el par (" << p << ", " << q << ") peso "<<w<<" al MatrixGraph" << endl;
		}
		
	}
	
	cout << "=========================" << endl;
	cout << "Grafo armado: " << endl;
	cout << "=========================" << endl;
	wg.printList();
	
	cout << "=========================" << endl;
	cout << "Dijsktra: " << endl;
	cout << "=========================" << endl;
	wg.dijsktra(0);
	
	cout << "=========================" << endl;
	cout << "Kruskal: " << endl;
	cout << "=========================" << endl;
	wg.kruskal();
	
	return 0;
}