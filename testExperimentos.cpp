#include <iostream>
#include <chrono>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include "libs/Hash.hpp"
#include "libs/ArbolBinarioBusqueda.hpp"

using namespace std;

int main(int argc, char * argv[]){
	if(argc != 2){
		cout << "ERROR!! USE " << argv[0] << " <FILENAME> donde:\n"
			 <<"<FILENAME> nombre del archivo a leer \n";
		return -1;
	}
	char * filename = argv[1];	
	ifstream f;
	f.open(filename);
	if(!f.is_open()){
		cout << "ERROR!!! el archivo " << filename << " no se pudo abrir\n";
		return -1;
	}
	
	auto start = chrono::high_resolution_clock::now();
	auto finish = chrono::high_resolution_clock::now();
	long tArbol[6];	// Tiempo primeros 50, 100, 500, 1000, 5000 y 10000 elementos
	long tHash[6];
	int tElem[] = {50, 100, 500, 1000, 5000, 10000};
	ArbolBinarioBusqueda arbol;
	Hash h;
	
	/******************************************************************************
		TIEMPO INSERCION ARBOL BINARIO DE BUSQUEDA
	******************************************************************************/
	
	int cantidad, valor;
	long tiempo = 0;
	f >> cantidad;
	for(int i = 0; i < cantidad; i++){
		f >> valor;
		start = chrono::high_resolution_clock::now();
		arbol.insertar(valor);
		finish = chrono::high_resolution_clock::now();
		tiempo += chrono::duration_cast<chrono::microseconds> (finish - start).count();
		if(i == tElem[0]-1){	// insertar 50°
			tArbol[0] = tiempo;
		}else if(i == tElem[1]-1){	// insertar 100°
			tArbol[1] = tiempo;
		}else if(i == tElem[2]-1){	// insertar 500°
			tArbol[2] = tiempo;
		}else if(i == tElem[3]-1){	// insertar 1000°
			tArbol[3] = tiempo;
		}else if(i == tElem[4]-1){	// insertar 5000°
			tArbol[4] = tiempo;
		}else if(i == tElem[5]-1){	// insertar 10000°
			tArbol[5] = tiempo;
		}
	}
	f.close();
	
	/******************************************************************************
		TIEMPO INSERCION HEAP
	******************************************************************************/
	
	f.open(filename);
	f >> cantidad;
	tiempo = 0;
	//start = chrono::high_resolution_clock::now();
	for(int i = 0; i < cantidad; i++){
		f >> valor;
		start = chrono::high_resolution_clock::now();
		h.insertar(valor);
		finish = chrono::high_resolution_clock::now();
		tiempo += chrono::duration_cast<chrono::microseconds> (finish - start).count();
		if(i == tElem[0]-1){
			tHash[0] = tiempo;
		}else if(i == tElem[1]-1){
			tHash[1] = tiempo;
		}else if(i == tElem[2]-1){
			tHash[2] = tiempo;
		}else if(i == tElem[3]-1){
			tHash[3] = tiempo;
		}else if(i == tElem[4]-1){
			tHash[4] = tiempo;
		}else if(i == tElem[5]-1){
			tHash[5] = tiempo;
		}
	}
	
	f.close();
	
	cout << "INSERCIONES: "  << endl;
	
	cout << "insertados \t Arbol [us] \t Hash [us]" << endl;
	for(int i = 0; i < 6; i++){
		cout << tElem[i] << "\t\t" << tArbol[i] << "\t\t" << tHash[i] << endl;
	}
	
	/******************************************************************************
		TIEMPO BUSQUEDA ARBOL BINARIO DE BUSQUEDA
	******************************************************************************/
	
	int n = 1000;
	srand(time(NULL));
	int elemento;
	long tArbolExistente = 0;
	long tArbolNoExistente = 0;
	long tHashExistente = 0;
	long tHashNoExistente = 0;
	
	for(int i = 0; i < n; i++){
		elemento = (int)rand() % cantidad;	// Busqueda de un valor que existe
		start = chrono::high_resolution_clock::now();
		arbol.buscar(elemento);
		finish = chrono::high_resolution_clock::now();
		tArbolExistente += chrono::duration_cast<chrono::nanoseconds> (finish - start).count();
	}
	tArbolExistente /= n;
	
	for(int i = 0; i < n; i++){
		elemento = (int)rand() % cantidad + cantidad;	// Busqueda de un valor que NO existe
		start = chrono::high_resolution_clock::now();
		arbol.buscar(elemento);
		finish = chrono::high_resolution_clock::now();
		tArbolNoExistente += chrono::duration_cast<chrono::nanoseconds> (finish - start).count();
	}
	tArbolNoExistente /= n;
	
	/******************************************************************************
		TIEMPO BUSQUEDA HASH
	******************************************************************************/
	
	for(int i = 0; i < n; i++){
		elemento = (int)rand() % cantidad;	// Busqueda de un valor que existe
		start = chrono::high_resolution_clock::now();
		h.buscar(elemento);
		finish = chrono::high_resolution_clock::now();
		tHashExistente += chrono::duration_cast<chrono::nanoseconds> (finish - start).count();
	}
	tHashExistente /= n;
	
	for(int i = 0; i < n; i++){
		elemento = (int)rand() % cantidad + cantidad;	// Busqueda de un valor que NO existe
		start = chrono::high_resolution_clock::now();
		h.buscar(elemento);
		finish = chrono::high_resolution_clock::now();
		tHashNoExistente += chrono::duration_cast<chrono::nanoseconds> (finish - start).count();
	}
	tHashNoExistente /= n;
	
	cout << "BUSQUEDAS: " << endl;
	cout << "Tipo \t Arbol [ns] \t Hash [ns]" << endl;
	cout << "Existente \t" << tArbolExistente << "\t" << tHashExistente << endl;
	cout << "No existente \t" << tArbolNoExistente << "\t" << tHashNoExistente << endl;
	
	return 0;
}