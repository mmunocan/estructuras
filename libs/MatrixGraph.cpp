#include "MatrixGraph.hpp"

MatrixGraph::MatrixGraph(int nodos){
	this->nodos = nodos;
	matrix = new bool*[nodos];
	for(int i = 0; i < nodos; i++){
		matrix[i] = new bool[nodos];
		for(int j = 0; j < nodos; j++){
			matrix[i][j] = false;
		}
	}
}

MatrixGraph::~MatrixGraph(){
	for(int i = 0; i < nodos; i++){
		delete [] matrix[i];
	}
	delete [] matrix;
}

bool MatrixGraph::insertar(int p, int q){
	if(p >= nodos || q >= nodos) return false; // no se inserto fuera de rango
	if(matrix[p][q]) return false; // arista ya insertada
	matrix[p][q] = true;
	return true;
}

bool MatrixGraph::checkLink(int p, int q){
	if(p >= nodos || q >= nodos) return false; // fuera de rango
	if(matrix[p][q]) return true;	// SE encuentra el enlace
	return false;	// No se encuentra el enlace
}

vector<int> * MatrixGraph::vecinosDirectos(int p){
	if(p >= nodos) return NULL; // fuera de rango
	vector<int> * output = new vector<int>();
	for(int q = 0; q < nodos; q++){
		if(matrix[p][q]) output->push_back(q);
	}
	return output;
}

vector<int> * MatrixGraph::vecinosReversos(int q){
	if(q >= nodos) return NULL; // fuera de rango
	vector<int> * output = new vector<int>();
	for(int p = 0; p < nodos; p++){
		if(matrix[p][q]) output->push_back(p);
	}
	return output;
}

void MatrixGraph::BFS(){
	bool visitados[nodos];
	bool encolados[nodos];
	for(int i = 0; i < nodos; i++){
		visitados[i] = false;
		encolados[i] = false;
	}
	queue<int> cola;
	cola.push(0);
	encolados[0] = true;
	
	while(!cola.empty()){
		int nodo = cola.front();
		cola.pop();
		cout << nodo << " - ";
		visitados[nodo] = true;
		encolados[nodo] = false;
		vector<int> vecinos = *(vecinosDirectos(nodo));
		for(int i = 0; i < vecinos.size(); i++){
			if(!visitados[vecinos[i]] && !encolados[vecinos[i]]){
				cola.push(vecinos[i]);
				encolados[vecinos[i]] = true;
			}
		}
	}
	cout << endl;
}

void MatrixGraph::DFS(){
	bool * visitados = new bool[nodos];
	for(int i = 0; i < nodos; i++){
		visitados[i] = false;
	}
	DFS_recursivo(0, visitados);
	cout << endl;
}

void MatrixGraph::DFS_recursivo(int nodo, bool * visitados){
	cout << nodo << " - ";
	visitados[nodo] = true;
	vector<int> vecinos = *(vecinosDirectos(nodo));
	for(int i = 0; i < vecinos.size(); i++){
		if(!visitados[vecinos[i]]){
			DFS_recursivo(vecinos[i], visitados);
		}
	}
}

void MatrixGraph::printMatrix(){
	for(int i = 0; i < nodos; i++){
		for(int j = 0; j < nodos; j++){
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
}

void MatrixGraph::componentesConexas(){
	stack<int> pila;
	DFS_privado(pila);
	
	MatrixGraph reverso = *(getReverse());
	
	bool * visitados = new bool[nodos];
	for(int i = 0; i < nodos; i++) visitados[i] = false;
	
	while(!pila.empty()){
		int tope = pila.top();
		pila.pop();
		if(!visitados[tope]){
			cout << "Componente conexa: " << endl;
			reverso.DFS_recursivo(tope, visitados);
			cout << endl;
		}
	}
}

void MatrixGraph::DFS_privado(stack<int> &pila){
	bool * visitados = new bool[nodos];
	for(int i = 0; i < nodos; i++) visitados[i] = false;
	for(int i = 0; i < nodos; i++){
		if(!visitados[i]){
			DFS_visitar_privado(i, visitados, pila);
		}
	}
	delete [] visitados;
}

void MatrixGraph::DFS_visitar_privado(int n, bool * visitados, stack<int> &pila){
	visitados[n] = true;
	vector<int> vecinos = *(vecinosDirectos(n));
	for(int i = 0; i < vecinos.size(); i++){
		if(!visitados[vecinos[i]]){
			DFS_visitar_privado(vecinos[i], visitados, pila);
		}
	}
	pila.push(n);
}

MatrixGraph * MatrixGraph::getReverse(){
	MatrixGraph * reverse = new MatrixGraph(nodos);
	vector<int> vecinos;
	for(int i = 0; i < nodos; i++){
		vecinos = *(vecinosDirectos(i));
		for(int j = 0; j < vecinos.size(); j++){
			reverse->insertar(vecinos[j], i);
		}
	}
	return reverse;
}