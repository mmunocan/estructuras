#include "LinkedGraph.hpp"


#include "LinkedGraph.hpp"

LinkedGraph::LinkedGraph(int nodos){
	this->nodos = nodos;
	lista = new vector<int>[nodos];
}

LinkedGraph::~LinkedGraph(){
	delete [] lista;
}

bool LinkedGraph::insertar(int p, int q){
	if(p >= nodos || q >= nodos) return false; // no se inserto fuera de rango
	for(int i = 0; i < lista[p].size(); i++){
		if(lista[p][i] == q) return false; // arista ya insertada
	}
	lista[p].push_back(q);
	return true;
}

bool LinkedGraph::checkLink(int p, int q){
	if(p >= nodos || q >= nodos) return false; // fuera de rango
	for(int i = 0; i < lista[p].size(); i++){
		if(lista[p][i] == q) return true;
	}
	return false;
}

vector<int> * LinkedGraph::vecinosDirectos(int p){
	if(p >= nodos) return NULL; // fuera de rango
	return &(lista[p]);
}

vector<int> * LinkedGraph::vecinosReversos(int q){
	if(q >= nodos) return NULL; // fuera de rango
	vector<int> * output = new vector<int>();
	for(int p = 0; p < nodos; p++){
		for(int j = 0; j < lista[p].size(); j++){
			if(lista[p][j] == q){
				output->push_back(p);
				break;
			}
		}
		
	}
	return output;
}

void LinkedGraph::BFS(){
	bool visitados[nodos];
	bool encolados[nodos];
	for(int i = 0; i < nodos; i++){
		visitados[i] = false;
		encolados[i] = false;
	}
	queue<int> cola;
	cola.push(0);
	encolados[0] = true;
	
	while(!cola.empty()){
		int nodo = cola.front();
		cola.pop();
		cout << nodo << " - ";
		visitados[nodo] = true;
		encolados[nodo] = false;
		vector<int> vecinos = *(vecinosDirectos(nodo));
		for(int i = 0; i < vecinos.size(); i++){
			if(!visitados[vecinos[i]] && !encolados[vecinos[i]]){
				cola.push(vecinos[i]);
				encolados[vecinos[i]] = true;
			}
		}
	}
	cout << endl;
}

void LinkedGraph::DFS(){
	bool * visitados = new bool[nodos];
	for(int i = 0; i < nodos; i++){
		visitados[i] = false;
	}
	DFS_recursivo(0, visitados);
	cout << endl;
}

void LinkedGraph::DFS_recursivo(int nodo, bool * visitados){
	cout << nodo << " - ";
	visitados[nodo] = true;
	vector<int> vecinos = *(vecinosDirectos(nodo));
	for(int i = 0; i < vecinos.size(); i++){
		if(!visitados[vecinos[i]]){
			DFS_recursivo(vecinos[i], visitados);
		}
	}
}

void LinkedGraph::printList(){
	for(int i = 0; i < nodos; i++){
		cout << i << ": ";
		for(int j = 0; j < lista[i].size(); j++){
			cout << lista[i][j] << " - ";
		}
		cout << endl;
	}
}

void LinkedGraph::componentesConexas(){
	stack<int> pila;
	DFS_privado(pila);
	
	LinkedGraph reverso = *(getReverse());
	
	bool * visitados = new bool[nodos];
	for(int i = 0; i < nodos; i++) visitados[i] = false;
	
	while(!pila.empty()){
		int tope = pila.top();
		pila.pop();
		if(!visitados[tope]){
			cout << "Componente conexa: " << endl;
			reverso.DFS_recursivo(tope, visitados);
			cout << endl;
		}
	}
}

void LinkedGraph::DFS_privado(stack<int> &pila){
	bool * visitados = new bool[nodos];
	for(int i = 0; i < nodos; i++) visitados[i] = false;
	for(int i = 0; i < nodos; i++){
		if(!visitados[i]){
			DFS_visitar_privado(i, visitados, pila);
		}
	}
	delete [] visitados;
}

void LinkedGraph::DFS_visitar_privado(int n, bool * visitados, stack<int> &pila){
	visitados[n] = true;
	vector<int> vecinos = *(vecinosDirectos(n));
	for(int i = 0; i < vecinos.size(); i++){
		if(!visitados[vecinos[i]]){
			DFS_visitar_privado(vecinos[i], visitados, pila);
		}
	}
	pila.push(n);
}

LinkedGraph * LinkedGraph::getReverse(){
	LinkedGraph * reverse = new LinkedGraph(nodos);
	vector<int> vecinos;
	for(int i = 0; i < nodos; i++){
		vecinos = *(vecinosDirectos(i));
		for(int j = 0; j < vecinos.size(); j++){
			reverse->insertar(vecinos[j], i);
		}
	}
	return reverse;
}