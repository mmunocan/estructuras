#ifndef LINKEDGRAPH_H
#define LINKEDGRAPH_H

#include <iostream>
#include <vector>
#include <queue>
#include <stack>
#include <algorithm>

using namespace std;

class LinkedGraph{
public:
	LinkedGraph(int nodos);
	~LinkedGraph();
	bool insertar(int p, int q);
	bool checkLink(int p, int q);
	vector<int> * vecinosDirectos(int p);
	vector<int> * vecinosReversos(int q);
	void BFS();
	void DFS();
	
	void printList();
	
	void componentesConexas();
private:
	vector<int> * lista;
	int nodos;
	void DFS_recursivo(int nodo, bool * visitados);
	
	void DFS_privado(stack<int> &pila);
	void DFS_visitar_privado(int n, bool * visitados, stack<int> &pila);
	LinkedGraph * getReverse();
};

#endif