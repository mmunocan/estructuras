#include "Hash.hpp"


Hash::Hash(){
	capacidad = 53;	// Doble 106
	cantidad = 0;
	tabla = new int[capacidad];
	for(int i = 0; i < capacidad; i++){
		tabla[i] = DISP;
	}
	
}

Hash::~Hash(){
	delete [] tabla;
}


int Hash::buscar(int k){
	return DISP;
}

void Hash::eliminar(int k){
	
}

int Hash::getCantidad(){
	return cantidad;
}

void Hash::insertar(int k){
	if((float)cantidad/(float)capacidad > 0.7){	// si esta muy lleno, agrandar
		agrandar();
	}
	int pos;	
	pos = double_hashing(k, 0);
	int i = 1;
	while(tabla[pos] != DISP){
		pos = double_hashing(k+i, i);
		i++;
		
	}
	tabla[pos] = k;
	cantidad++;
}

void Hash::agrandar(){
	int aux[capacidad]; // Orig = 53
	// copiar todo lo de tabla a aux
	for(int i = 0; i < capacidad; i++) aux[i] =tabla[i];
	
	// Vaciar y duplicar tabla
	tabla = new int[capacidad * 2];
	for(int i = 0; i < capacidad * 2; i++){
		tabla[i] = DISP;
	}
	
	cantidad = 0;
	capacidad *= 2;
	// Reacomodar los elementos
	for(int i = 0; i < capacidad/2; i++){
		if(aux[i] != DISP){	// hay un elemento en esa pos
			insertar(aux[i]);
		}
	}
	
	
}

/**********************/
/*** Funciones hash ***/
/**********************/

// Método de la división
// k: clave a la cual aplicaremos la función hash
// n: tamaño de la tabla hash
int Hash::h1(int k) {
  return k%capacidad;
}


// Método de la multiplicación
// k: clave a la cual aplicaremos la función hash
// n: tamaño de la tabla hash
int Hash::h2(int k) {
  float a = (float)k*A;
  a -= (int)a; // Esta línea implementa la operación módulo 1 (%1)
  
  return capacidad*a;
}

int Hash::double_hashing(int k,  int i) {
  // Utilizando como primer método el método de la multiplicación y luego el
  // método de la división
  return (h2(k) + i*(h1(k)+1)) % capacidad;
}