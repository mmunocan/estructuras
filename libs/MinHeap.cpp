#include "MinHeap.hpp"

/*****************************************************
			Funciones publicas
*****************************************************/

MinHeap::MinHeap(){
	cant = 0;
	cap = 10;
	heap = new int[cap];
	heap[0] = -1;
}

MinHeap::~MinHeap(){
	delete [] heap;
}

void MinHeap::insert(int valor){
	cant++;
	heap[cant] = valor;
	int pos = cant;
	int papa = padre(pos);
	while(papa != 0 && heap[pos] < heap[papa]){
		swap(heap[pos], heap[papa]);
		pos = papa;
		papa = padre(pos);
	}
}

int MinHeap::buscarMin(){
	if(cant == 0)	return -1;
	return heap[1];
}

void MinHeap::borrarMin(){
	if(cant == 0) return;
	heap[1] = heap[cant];
	cant--;
	int papa = 1;
	int hijo;
	
	if(heap[hijoIzquierdo(papa)] < heap[hijoDerecho(papa)]){
		hijo = hijoIzquierdo(papa);
	}else{
		hijo = hijoDerecho(papa);
	}
	while(hijo <= cant && heap[papa] > heap[hijo]){
		swap(heap[papa], heap[hijo]);
		papa = hijo;
		if(heap[hijoIzquierdo(papa)] < heap[hijoDerecho(papa)]){
			hijo = hijoIzquierdo(papa);
		}else{
			hijo = hijoDerecho(papa);
		}
	}
}

/*****************************************************
			Funciones privadas
*****************************************************/

void MinHeap::duplicar(){
	cap = cap * 2;
	realloc(heap, cap * sizeof(int));
}

int MinHeap::hijoIzquierdo(int pos){
	return 2*pos;
}

int MinHeap::hijoDerecho(int pos){
	return (2*pos)+1;
}

int MinHeap::padre(int pos){
	if(pos % 2 == 0){	// Hijo izquierdo
		return pos / 2;
	}
	return (pos-1) / 2;	// Hijo derecho
	
}

