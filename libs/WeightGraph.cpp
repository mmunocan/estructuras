#include "WeightGraph.hpp"

WeightGraph::WeightGraph(int nodos){
	this->nodos = nodos;
	lista = new vector<pair<int,int>>[nodos];
}

WeightGraph::~WeightGraph(){
	delete [] lista;
}

bool WeightGraph::insertar(int p, int q, int w){
	if(p >= nodos || q >= nodos) return false;
	for(int i = 0; i < lista[p].size(); i++){
		if(lista[p][i].second == q){ // El elemento ya existe, cambiar distancia
			lista[p][i].first = w;
			return true;
		}
	}
	lista[p].push_back(make_pair(w,q));
	
	// Manejo de las Arista
	Arista arista;
	arista.a = p;
	arista.b = q;
	arista.costo = w;
	listaArista.push_back(arista);
	sort(listaArista.begin(), listaArista.end());
	
	return true;
}

void WeightGraph::printList(){
	for(int i = 0; i < nodos; i++){
		cout << i << ": ";
		for(int j = 0; j < lista[i].size(); j++){
			cout << lista[i][j].second << "(" << lista[i][j].first << ")" << " - ";
		}
		cout << endl;
	}
}

// pair<int,int> = (distancia, nodo)
void WeightGraph::dijsktra(int inicio){
	vector<bool> visitados(nodos, false);
	/*
		equivalente a:
		bool visitados[nodos];
		for(int i = 0; i< nodos; i++) visitados[i] = false;
	*/
	
	// Tablero de etiquetas
	vector<pair<int, int>> dijsktra(nodos, make_pair(INT_MAX, -1));
	dijsktra[inicio].first = 0;
	dijsktra[inicio].second = inicio;
	
	// Cola de prioridad
	// priority_queue<tipo dato, estructura subyacente, funcion prioridad> Q;
	priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> Q;
	Q.push(make_pair(0, inicio));
	
	pair<int, int> par, vecino;
	
	while(!Q.empty()){
		par = Q.top();
		Q.pop();
		visitados[par.second] = true;
		
		for(int i = 0; i < lista[par.second].size(); i++){	// Iterar por vecinos directos
			vecino = lista[par.second][i];
			if(!visitados[vecino.second]){	// Nodo aun no zanjado
				int nuevaDistancia = par.first + vecino.first;
				int antiguaDistancia = dijsktra[vecino.second].first;
				if(nuevaDistancia < antiguaDistancia){	// Encontre una distancia mas corta
					// Actualizar las etiquetas
					dijsktra[vecino.second].first = nuevaDistancia;
					dijsktra[vecino.second].second = par.second;
					// inserto en la cola
					Q.push(make_pair(nuevaDistancia, vecino.second));
				}
			}
		}
		
	}
	
	cout << "DIJSTRA: " << endl;
	
	for(int i = 0; i < nodos; i++){
		cout << dijsktra[i].first << " " << dijsktra[i].second << endl;
	}
	
}


void WeightGraph::kruskal(){
	vector<Arista> AristaKruskal;
	UnionFind unionFind(nodos);
	Arista miArista;
	
	for(int i = 0; i < listaArista.size(); i++){
		miArista = listaArista[i];
		if(!unionFind.sameset(miArista.a, miArista.b)){
			AristaKruskal.push_back(miArista);
			unionFind.union_(miArista.a, miArista.b);
		}
	}
	
	cout << "Kruskal:" << endl;
	for(int i = 0; i < AristaKruskal.size(); i++){
		cout << "(" << AristaKruskal[i].a << ", " << AristaKruskal[i].b << ") -> " << AristaKruskal[i].costo << endl;
	}
	
}