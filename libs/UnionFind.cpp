#include "UnionFind.hpp"

UnionFind::UnionFind(int n){
	p = new int[n];
	rank = new int[n];
	for(int i = 0; i < n; i++){
		p[i] = i;
		rank[i] = 0;
	}
}

UnionFind::~UnionFind(){
	delete [] p;
	delete [] rank;
}

int UnionFind::find(int i){
	if(p[i] == i){
		return i;	// Se autorepresenta
	}
	return find(p[i]);
}

bool UnionFind::sameset(int i, int j){
	return find(i) == find(j);
}

void UnionFind::union_(int i, int j){
	if(!sameset(i, j)){
		int rep_i = find(i);
		int rep_j = find(j);
		int rank_i = rank[rep_i];
		int rank_j = rank[rep_j];
		if(rank_i > rank_j){
			p[rep_j] = rep_i;
		}else{
			p[rep_i] = rep_j;
			if(rank[rep_i] == rank[rep_j]){
				rank[rep_j]++;
			}
		}
	}
}