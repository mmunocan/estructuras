#include "ArbolBinarioBusqueda.hpp"

/*******************************************
		Clase Nodo
********************************************/

Nodo::Nodo(){
	izquierdo = NULL;
	derecho = NULL;
	padre = NULL;
}

Nodo::Nodo(int valor){
	izquierdo = NULL;
	derecho = NULL;
	padre = NULL;
	this->valor = valor;
}

Nodo::Nodo(int valor, Nodo * padre){
	izquierdo = NULL;
	derecho = NULL;
	this->padre = padre;
	this->valor = valor;
}

Nodo::~Nodo(){
	delete izquierdo;
	delete derecho;
}

/*******************************************
		Clase ArbolBinarioBusqueda
********************************************/

ArbolBinarioBusqueda::ArbolBinarioBusqueda(){
	raiz = NULL;
}

ArbolBinarioBusqueda::~ArbolBinarioBusqueda(){
	delete raiz;
}


bool ArbolBinarioBusqueda::insertar(int valor){
	if(raiz == NULL){
		raiz = new Nodo(valor);
		return true;	// Insertamos en la raiz
	}	
	return insertar(valor, raiz);
}

// n != NULL
bool ArbolBinarioBusqueda::insertar(int valor, Nodo * n){
	// Caso base
	if(valor == n->valor){
		return false;	// No se aceptan repetidos
	}
	if(valor < n->valor){
		if(n->izquierdo == NULL){	// Caso base
			n->izquierdo = new Nodo(valor, n);
			return true;
		}
		// caso recursivo
		return insertar(valor, n->izquierdo);
	}
	
	// n->valor < valor
	if(n->derecho == NULL){	// Caso base
		n->derecho = new Nodo(valor, n);
		return true;
	}
	// caso recursivo
	return insertar(valor, n->derecho);
}


Nodo * ArbolBinarioBusqueda::buscar(int valor){
	buscar(valor, raiz);
}

Nodo * ArbolBinarioBusqueda::buscar(int valor, Nodo * n){
	// Caso base
	if(n == NULL){	// No existe
		return NULL;
	}
	if(n->valor == valor){ // El valor lo encontramos
		return n;
	}
	// Casos recursivos
	if(valor < n->valor){
		return buscar(valor, n->izquierdo);
	}
	
	// n->valor < valor
	return buscar(valor, n->derecho);
}

void ArbolBinarioBusqueda::inOrder(){
	cout << "In Order:" << endl;
	inOrder(raiz);
	cout << endl;
}

void ArbolBinarioBusqueda::inOrder(Nodo * n){
	// Caso base
	if(n == NULL){
		return;
	}
	inOrder(n->izquierdo);
	cout << n->valor << " ";
	inOrder(n->derecho);
	
	
}

Nodo * ArbolBinarioBusqueda::predecesor(int valor){
	Nodo * n = buscar(valor, raiz);
	if(n == NULL){	// No existe el valo
		return NULL;
	}
	if(n->izquierdo != NULL){
		return mayor(n->izquierdo);
	}
	
	Nodo * padre = n->padre;
	while(padre != NULL && padre->izquierdo == n){
		n = n->padre;
		padre = padre->padre;
	}
	return padre;
}

Nodo * ArbolBinarioBusqueda::mayor(Nodo * n){
	if(n == NULL){
		return NULL;
	}
	
	Nodo * it = n;
	while(it->derecho != NULL){
		it = it->derecho;
	}
	return it;
}