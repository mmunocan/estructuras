#ifndef WEIGHTGRAPH_H
#define WEIGHTGRAPH_H

#include <iostream>
#include <vector>
#include <queue>
#include <utility>
#include <algorithm>
#include <climits>

using namespace std;

#include "UnionFind.hpp"

struct Arista{
	int a;	// origen
	int b;	// destino
	int costo;
	bool operator<(const Arista e) const{
		return costo < e.costo;
	}
};

class WeightGraph{
public:
	WeightGraph(int nodos);
	~WeightGraph();
	bool insertar(int p, int q, int w);
	void printList();
	void dijsktra(int inicio);
	void kruskal();
private:
	vector<pair<int,int>> * lista;
	vector<Arista>listaArista;
	int nodos;
	
};

#endif