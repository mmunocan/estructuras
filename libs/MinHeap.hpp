#ifndef MINHEAP_H
#define MINHEAP_H

#include <utility>	// Para swap
#include <cstdlib>	// Para realloc

#include <iostream>

using namespace std;

class MinHeap{
public:
	MinHeap();
	~MinHeap();
	void insert(int valor);	
	int buscarMin(); // Si es -1 no existe
	void borrarMin();
private:
	int * heap;
	int cant;
	int cap;
	void duplicar();
	int hijoIzquierdo(int pos);
	int hijoDerecho(int pos);
	int padre(int pos);
};

#endif