#ifndef UNIONFIND_H
#define UNIONFIND_H

using namespace std;


class UnionFind{
public:
	UnionFind(int n);
	~UnionFind();
	int find(int i);
	bool sameset(int i, int j);
	void union_(int i, int j);
private:
	int * p;
	int * rank;
};

#endif