#ifndef ARBOLBINARIOBUSQUEDA_H
#define ARBOLBINARIOBUSQUEDA_H

#include <cstddef>
#include <iostream>

using namespace std;

class Nodo{
public:
	int valor;
	Nodo * izquierdo;
	Nodo * derecho;
	Nodo * padre;
	Nodo();
	Nodo(int valor);
	Nodo(int valor, Nodo * padre);
	~Nodo();
private:
	
};

class ArbolBinarioBusqueda{
public:
	ArbolBinarioBusqueda();
	~ArbolBinarioBusqueda();
	bool insertar(int valor);	// No se aceptan repetidos 
	Nodo * buscar(int valor);	// Si valor no existe, return NULL
	void inOrder();		// De tarea: preorder y posorder
	Nodo * predecesor(int valor);	// NULL si valor no existe o es el menor
	// Eliminar 
private:
	Nodo * raiz;
	bool insertar(int valor, Nodo * n);
	Nodo * buscar(int valor, Nodo * n);
	void inOrder(Nodo * n);
	Nodo * mayor(Nodo * n);
};

#endif