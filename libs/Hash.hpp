#include <string>
#include <iostream>
#include <cmath>

#define DISP -1

#ifndef HASH_HPP
#define HASH_HPP

using namespace std;

class Hash{
private:
	int * tabla;
	int capacidad;
	int cantidad;
	float A = (sqrt(5) - 1) / 2;
	int h1(int k);
	int h2(int k);
	int double_hashing(int k,  int i);
	void agrandar();
public:
	Hash();
	~Hash();
	int getCantidad();
	int buscar(int k);	// retornar posicion
	void eliminar(int k);
	void insertar(int k);	// Numeros > 0
};

#endif