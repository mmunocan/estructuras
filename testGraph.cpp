#include <iostream>
#include <fstream>
#include "libs/MatrixGraph.hpp"
#include "libs/LinkedGraph.hpp"

using namespace std;

int main(int argc, char * argv[]){
	if(argc != 2){
		cout << "USO: " << argv[0] << " <FILENAME>" << endl;
		return -1;
	}
	ifstream input(argv[1]);
	if(!input.is_open()){
		cout << "Error al abrir el archivo" << argv[1] << endl;
		return -1;
	}
	
	int nodos, p, q;
	input >> nodos;
	
	MatrixGraph mg(nodos);
	LinkedGraph lg(nodos);
	
	cout << "=========================" << endl;
	cout << "Inserciones: " << endl;
	cout << "=========================" << endl;
	
	while(input >> p >> q){
		if(mg.insertar(p, q)){
			cout << "Inserté el par (" << p << ", " << q << ") al MatrixGraph" << endl;
		}else{
			cout << "No pude insertar el par (" << p << ", " << q << ") al MatrixGraph" << endl;
		}
		
		if(lg.insertar(p, q)){
			cout << "Inserté el par (" << p << ", " << q << ") al LinkedGraph" << endl;
		}else{
			cout << "No pude insertar el par (" << p << ", " << q << ") al LinkedGraph" << endl;
		}
	}
	
	cout << "=========================" << endl;
	cout << "Matriz de adyacencia: " << endl;
	cout << "=========================" << endl;
	mg.printMatrix();
	
	cout << "=========================" << endl;
	cout << "Lista de adyacencia " << endl;
	cout << "=========================" << endl;
	lg.printList();
	
	cout << "=========================" << endl;
	cout << "Chequeo de enlaces: " << endl;
	cout << "=========================" << endl;
	cout << "Matriz de adyacencia: " << endl;
	cout << "=========================" << endl;
	
	for(int i = 0; i < nodos; i++){
		for(int j = 0; j < nodos; j++){
			if(mg.checkLink(i,j)){
				cout << "Encontré el par (" << i << ", " << j << ")" << endl;
			}
		}
	}
	
	cout << "=========================" << endl;
	cout << "Lista de adyacencia " << endl;
	cout << "=========================" << endl;
	
	for(int i = 0; i < nodos; i++){
		for(int j = 0; j < nodos; j++){
			if(lg.checkLink(i,j)){
				cout << "Encontré el par (" << i << ", " << j << ")" << endl;
			}
		}
	}
	
	cout << "=========================" << endl;
	cout << "Vecinos directos: " << endl;
	cout << "=========================" << endl;
	cout << "Matriz de adyacencia: " << endl;
	cout << "=========================" << endl;
	
	vector<int> v;
	
	for(int i = 0; i < nodos; i++){
		cout << "Vecino de " << i << ": ";
		v = *(mg.vecinosDirectos(i));
		for(int j = 0; j < v.size(); j++){
			cout << v[j] << " - ";
		}
		cout << endl;
	}
	
	cout << "=========================" << endl;
	cout << "Lista de adyacencia " << endl;
	cout << "=========================" << endl;
	
	for(int i = 0; i < nodos; i++){
		cout << "Vecino de " << i << ": ";
		v = *(lg.vecinosDirectos(i));
		for(int j = 0; j < v.size(); j++){
			cout << v[j] << " - ";
		}
		cout << endl;
	}
	
	cout << "=========================" << endl;
	cout << "Vecinos reversos: " << endl;
	cout << "=========================" << endl;
	cout << "Matriz de adyacencia: " << endl;
	cout << "=========================" << endl;
	
	for(int i = 0; i < nodos; i++){
		cout << "Reverso de " << i << ": ";
		v = *(mg.vecinosReversos(i));
		for(int j = 0; j < v.size(); j++){
			cout << v[j] << " - ";
		}
		cout << endl;
	}
	
	cout << "=========================" << endl;
	cout << "Lista de adyacencia " << endl;
	cout << "=========================" << endl;
	
	for(int i = 0; i < nodos; i++){
		cout << "Reverso de " << i << ": ";
		v = *(lg.vecinosReversos(i));
		for(int j = 0; j < v.size(); j++){
			cout << v[j] << " - ";
		}
		cout << endl;
	}
	
	
	cout << "=========================" << endl;
	cout << "BFS : (Breadth First Search)" << endl;
	cout << "=========================" << endl;
	cout << "=========================" << endl;
	cout << "Matriz de adyacencia: " << endl;
	cout << "=========================" << endl;
	mg.BFS();
	
	cout << "=========================" << endl;
	cout << "Lista de adyacencia " << endl;
	cout << "=========================" << endl;
	lg.BFS();
	
	cout << "=========================" << endl;
	cout << "DFS (Depth First Search) : " << endl;
	cout << "=========================" << endl;
	cout << "=========================" << endl;
	cout << "Matriz de adyacencia: " << endl;
	cout << "=========================" << endl;
	mg.DFS();
	
	cout << "=========================" << endl;
	cout << "Lista de adyacencia " << endl;
	cout << "=========================" << endl;
	lg.DFS();
	
	cout << "=========================" << endl;
	cout << "Componentes conexas : " << endl;
	cout << "=========================" << endl;
	cout << "=========================" << endl;
	cout << "Matriz de adyacencia: " << endl;
	cout << "=========================" << endl;
	mg.componentesConexas();
	
	cout << "=========================" << endl;
	cout << "Lista de adyacencia " << endl;
	cout << "=========================" << endl;
	lg.componentesConexas();
	
	return 0;
}